import { Component, OnInit } from '@angular/core';
import { WeatherService } from './weather.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'weather';
    temperature: number = 0;
    feelsLikeTemp: number = 0;
    humidity: number = 0;
    pressure: number = 0;
    summary: string = '';
    city: string = '';
    name: string = '';
    description: string = "nil";
    isExist:boolean=false;

    constructor(private weatherService: WeatherService) { }

    ngOnInit() {
    }

    data: any;

    getCurrent() {
        this.getWeather(this.city);
    }

    async getWeather(city: string) {
        try {
            this.data = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&appid=fea2a884be74c38aa63794edac9ca416&units=imperial`).then(res => res.json())

            this.temperature = (this.data)?.main.temp;
            this.feelsLikeTemp = (this.data)?.main.temp_max;
            this.humidity = (this.data)?.main.humidity;
            this.name = (this.data)?.name;
            this.pressure = (this.data)?.main.pressure;
            this.summary = (this.data)?.weather[0].main;
            this.description = (this.data)?.weather[0].description;
            this.isExist=false;

        } catch (error) {
            console.log("Error occured while fetching data");
            this.isExist=true;
        }


    }

}
